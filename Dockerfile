FROM python:3.8

WORKDIR /digitized_scores_app
ADD digitized_scores_app /digitized_scores_app/
ADD requirements.txt /digitized_scores_app/
RUN pip install -r requirements.txt
WORKDIR /
ADD setup.py /
RUN pip install -e .
ENV FLASK_APP digitized_scores_app
ENV FLASK_ENV production
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["/digitized_scores_app/main.py"]
