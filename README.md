# Making digitised scores audible

GlamHack 2021 Project

See https://hack.glam.opendata.ch/project/71 

#Dependencies

See Dockerfile for dependencies.

# Start the app
```
python3 -m venv venv
. venv/bin/activate
pip3 install -r requirements.txt
pip3 install -e .
python3 main.py
cd ..
export FLASK_APP=digitized_scores_app
export FLASK_ENV=development
flask run
```
This will start a development web server hosting your application, which you will be able to see by navigating to http://127.0.0.1:5000/. 

