from flask import Flask, send_from_directory, redirect
from flask_restful import Api
from flasgger import Swagger

from digitized_scores_app.resources.Convert import Convert
from digitized_scores_app.helpers.Error import DigitizedScoresError
import os


def create_app(test_config=None):
    app = Flask(__name__)
    #app.config.from_object(digitized_scores_app.configuration)
    # app.config.from_envvar('YOURAPPLICATION_SETTINGS')

    # Based on this example :
    # https://github.com/flasgger/flasgger/blob/master/examples/restful.py

    api = Api(app)

    try:
        app.config['SWAGGER'] = {
            'title': 'Convert Scores',
            'version': 'dev',
            'uiversion': 3,
            'description': 'Convert digitized scores to mp3 and more',
            'contact': {
                'name': 'UB Basel',
                'url': 'https://ub.unibas.ch',
                'email': 'swissbib-ub@unibas.ch'},
            'favicon': '/favicon.ico'}
        Swagger(app)
    except Exception as ex:
        raise DigitizedScoresError(str(ex))

    @app.route("/")
    def home():
        return redirect("/apidocs")

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(
            os.path.join(
                app.root_path,
                'assets'),
            'favicon.ico',
            mimetype='image/vnd.microsoft.icon')

    api.add_resource(
        Convert,
        '/v1/convert/'
    )

    return app
