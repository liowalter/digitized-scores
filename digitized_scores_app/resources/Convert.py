import hashlib
import os

from flasgger import swag_from
from flask_restful import Resource, reqparse, current_app
from flask import request


class Convert(Resource):
    # Todo validate requests
    # @swag.validate('job-parameters')
    @swag_from('Convert.yml')
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('job-parameters', type=dict)
        args = parser.parse_args()
        job_parameters = args['job-parameters']
        image = job_parameters['image-url']

        return {'status': 'SUCCESS', 'message': image}, 201
