from setuptools import setup

setup(
    name='digitized_scores_app',
    packages=['digitized_scores_app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
